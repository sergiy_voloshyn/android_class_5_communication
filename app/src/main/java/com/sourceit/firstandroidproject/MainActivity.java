package com.sourceit.firstandroidproject;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
        implements ActivityCommunication {


    LeftFragment leftFragment;
    RightFragment rightFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        leftFragment = LeftFragment.newInstance();
        rightFragment = RightFragment.newInstance();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_left, leftFragment)
                .replace(R.id.container_right, rightFragment)
                .commit();

    }

    @Override
    public void updateText(String str) {
        rightFragment.setText(str);
    }
}
